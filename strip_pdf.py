#!/usr/bin/python3

## This code is released by Ralf Ulrich, ralf.ulrich@cern.ch, in May 2020 
## under the GPLv3 license. Use at your own risk! 

import re, sys, os
import subprocess

def is_tool(name):
    from shutil import which
    return which(name) is not None


if not is_tool('qpdf') or not is_tool('fix-qdf'):
    print ('This tool depends on the \"qpdf\" and \"fix-qdf\" tools. Please install first!')
    sys.exit(1)

if (len (sys.argv) != 2):
    print ('Please pass name of pdf file to remove page groups from.')
    sys.exit(1)
    
print (sys.argv)
filename = sys.argv[1]
filename_name, filename_extension = os.path.splitext(filename)
if filename_extension != ".pdf":
    print ("Can only handle pdf input files.")
    sys.exit(1)
outfilename = filename_name + '.strip.pdf'
outfile = open(outfilename, 'xb')

qpdf = subprocess.Popen(['qpdf', '--qdf', filename, '-'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
fixpdf = subprocess.Popen('fix-qdf', stdin=subprocess.PIPE, stdout=outfile, stderr=subprocess.PIPE)

output, err = qpdf.communicate()


page_group = None
for line in output:
    print (type(line), line)
    if page_group is None:
        if line.rstrip() == b"  /Group <<":
            page_group = [line]
        else:
            fixpdf.stdin.write(line)
    else:
        page_group.append(line)
        if line.rstrip() == b"  >>":
            break
else:
    if page_group:
        fixpdf.stdin.write(b"".join(page_group))
        page_group = None

for line in output:
    fixpdf.stdin.write(line)
fixpdf.stdin.flush()

fixout,fixerr = fixpdf.communicate()

print (fixout, fixerr)

if page_group:
    stderr.write(b"".join(page_group))
else:
    stderr.write(b"note: did not find page group\n")
